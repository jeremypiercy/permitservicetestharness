﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
// From HippoApi
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web;
using System.Net.Http.Headers;
using System.Net.Http.Formatting;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private HttpClient permitServiceClient;


        private void LiveButton_Click(object sender, EventArgs e)
        {
            permitServiceClient = new HttpClient()
            {
                BaseAddress = new Uri("https://temopermitservice.azurewebsites.net"),
                Timeout = TimeSpan.FromSeconds(250)
            };
            GetPermit(permitServiceClient);
        }
        
        private void LocalButton_Click(object sender, EventArgs e)
        {
            permitServiceClient = new HttpClient()
            {
                BaseAddress = new Uri("http://localhost:55086/"),
                Timeout = TimeSpan.FromSeconds(250)
            };
            GetPermit(permitServiceClient);
        }

        private async void GetPermit(HttpClient permitServiceClient)
        { 
            try
            {
                textBoxOutput.Text = "Working....";
                JObject permitPayload = JObject.Parse(textBoxJSONPackage.Text);
                
                //(permitPayload["JobDetails"] as JArray).RemoveAt(0);

                permitServiceClient.DefaultRequestHeaders.Add("appkey", "19EE5046-293C-4920-8F1D-D45D7B80998C");

                var permitResponse = await permitServiceClient.PostAsJsonAsync("api/v1/permit/generate", permitPayload);

                permitServiceClient.DefaultRequestHeaders.Remove("appkey");

                string permitResponseContent = await permitResponse.Content.ReadAsStringAsync();
                JObject permitResponseObject = JObject.Parse(permitResponseContent);


                if (!permitResponse.IsSuccessStatusCode || !string.IsNullOrWhiteSpace(permitResponseObject["PermitResponse"]?["ErrorCode"].Value<string>()))
                {
                    textBoxOutput.Text = "An error has occurred\n" + permitResponseContent;
                    //throw new Exception();
                }
                else
                    textBoxOutput.Text = "Sucess\n" + permitResponseContent;
                
                //  return Ok(new Dictionary<string, object> { { "Success", true }, { "Message", "Your permit documents have been emailed." + (actionDTO.IsDeveloperDebug ? "\n\n" + permitResponseObject.ToString() : "") } });
            }
            catch (Exception ex)
            {
                textBoxOutput.Text = "An exception was caught";
            }

        }
    }
}
