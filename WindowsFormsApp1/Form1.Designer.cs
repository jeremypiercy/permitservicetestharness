﻿namespace WindowsFormsApp1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.LiveButton = new System.Windows.Forms.Button();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.textBoxJSONPackage = new System.Windows.Forms.TextBox();
            this.textBoxOutput = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.LocalButton = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // LiveButton
            // 
            this.LiveButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.LiveButton.Location = new System.Drawing.Point(12, 30);
            this.LiveButton.Name = "LiveButton";
            this.LiveButton.Size = new System.Drawing.Size(75, 23);
            this.LiveButton.TabIndex = 0;
            this.LiveButton.Text = "Live Test";
            this.LiveButton.UseVisualStyleBackColor = true;
            this.LiveButton.Click += new System.EventHandler(this.LiveButton_Click);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // textBoxJSONPackage
            // 
            this.textBoxJSONPackage.Location = new System.Drawing.Point(12, 312);
            this.textBoxJSONPackage.MaxLength = 1000000;
            this.textBoxJSONPackage.Multiline = true;
            this.textBoxJSONPackage.Name = "textBoxJSONPackage";
            this.textBoxJSONPackage.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxJSONPackage.Size = new System.Drawing.Size(1362, 331);
            this.textBoxJSONPackage.TabIndex = 2;
            this.textBoxJSONPackage.Text = resources.GetString("textBoxJSONPackage.Text");
            // 
            // textBoxOutput
            // 
            this.textBoxOutput.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxOutput.Location = new System.Drawing.Point(12, 80);
            this.textBoxOutput.Multiline = true;
            this.textBoxOutput.Name = "textBoxOutput";
            this.textBoxOutput.ReadOnly = true;
            this.textBoxOutput.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxOutput.Size = new System.Drawing.Size(1362, 205);
            this.textBoxOutput.TabIndex = 3;
            // 
            // textBox1
            // 
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.Location = new System.Drawing.Point(12, 291);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(103, 15);
            this.textBox1.TabIndex = 4;
            this.textBox1.Text = "JSON data:";
            // 
            // LocalButton
            // 
            this.LocalButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.LocalButton.Location = new System.Drawing.Point(109, 31);
            this.LocalButton.Name = "LocalButton";
            this.LocalButton.Size = new System.Drawing.Size(75, 23);
            this.LocalButton.TabIndex = 5;
            this.LocalButton.Text = "Local Test";
            this.LocalButton.UseVisualStyleBackColor = true;
            this.LocalButton.Click += new System.EventHandler(this.LocalButton_Click);
            // 
            // textBox2
            // 
            this.textBox2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox2.Location = new System.Drawing.Point(12, 59);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(127, 15);
            this.textBox2.TabIndex = 6;
            this.textBox2.Text = "Permit Service Output:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1386, 659);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.LocalButton);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.textBoxOutput);
            this.Controls.Add(this.textBoxJSONPackage);
            this.Controls.Add(this.LiveButton);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button LiveButton;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.TextBox textBoxJSONPackage;
        private System.Windows.Forms.TextBox textBoxOutput;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button LocalButton;
        private System.Windows.Forms.TextBox textBox2;
    }
}

